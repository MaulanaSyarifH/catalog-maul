package com.trainingmicroservicemaul.userservices.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.trainingmicroservicemaul.userservices.entity.Product;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {
}

