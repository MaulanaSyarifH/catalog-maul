package com.trainingmicroservicemaul.userservices.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trainingmicroservicemaul.userservices.dao.ProductDao;
import com.trainingmicroservicemaul.userservices.entity.Product;

@RestController
public class ProductController {
	 @Autowired private ProductDao productDao;

	    @GetMapping("/api/product/")
	    public Iterable<Product> allProducts() {
	        return productDao.findAll();
	    }
}
